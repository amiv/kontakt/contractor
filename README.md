# Contractor

Simple tool to create AMIV job fair confirmation and the fair booklet
from CRM data.

The interface itself is based mainly on [flask](flask.pocoo.org) and uses 
[bootstrap v4](v4-alpha.getbootstrap.com)

## Configuration

A config file is needed to run the contractor.

If you are using the production AMIV API, you need to register an OAuth ID
first.
[(More Info)](https://github.com/amiv-eth/amivapi/blob/master/docs/OAuth.md)

Create the file, e.g. called `config.py`:

```python
# AMIVAPI & OAuth
#AMIVAPI_URL = 'https://api.amiv.ethz.ch/'
#OAUTH_ID = 'AMIV Contractor'
#OAUTH_URI = 'https://contractor.amiv.ethz.ch'

# Kontakt api
KONTAKTAPI_URL = 'https://api.kontakt.amiv.ethz.ch/contractor/'
KONTAKTAPI_KEY = 'put your secret key here'

# Signed sessions
SECRET_KEY = 'Generate something random!'

# DINPro URL
FONT_URL = 'https://wiki.amiv.ethz.ch/images/c/cb/DINPro.tar.gz'

# CORS - List of origins which are allowed to access the resources.
CORS_ORIGINS = [
    'https://api.kontakt.amiv.ethz.ch',
    'https://api.kontakt-dev.amiv.ethz.ch',
    'https://api.amiv.ethz.ch',
    'https://api-dev.amiv.ethz.ch',
]

# Sentry
#SENTRY_DSN =
```


## Deployment with Docker

A docker image is available under `notspecial/contractor` in the docker hub.
Both the config file and a URL to access the (non-public) DINPro
fonts are needed to run it. The URL can also be found in the
[AMIV Wiki](https://wiki.amiv.ethz.ch/Corporate_Design#DINPro).
Add the url to the config file as shown above.

The most convenient way to pass the config to the container is
using [Docker configs](https://docs.docker.com/engine/swarm/configs).

Set the target of the config to `/contractor/config.py` or set the
environment variable `CONTRACTOR_CONFIG` to the filename of your config
inside of the container.

## Development

For compilation of the tex files,
[amivtex](https://github.com/NotSpecial/amivtex) needs to be installed along
with the DINPro fonts. Take a look at the repository for instructions.

You need Python 3. The following commands create a virtual environment and
install dependencies:

```bash
# (If not yet done: clone and go into folder)
git clone https://github.com/NotSpecial/contractor
cd contractor

# Create and activate virtual environment
python -m venv env
source env/bin/activate

# Install dependencies
pip install -r requirements.txt

# Point app at config (optional, default is 'config.py' in current working dir)
export CONTRACTOR_CONFIG=...

# Start development server
export FLASK_APP=app.py
export FLASK_DEBUG=1
flask run
```

## Testing

There are some tests implemented, especially for tex creation and soap
connection. Use `py.test` to run them.

```
> pip install pytest
> py.test
```

The tests musst be run from the root directory (where the amivtex dir is) or 
the tex tests won't be able to find the .tex source.

*Beware:* The tex test tries a **lot** of choices and takes a lot of time to
finish!
