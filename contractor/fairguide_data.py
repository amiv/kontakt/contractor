"""Fairguide data. Eventuelly should be in CRM. Until then here."""

from os import path
from textwrap import dedent


def _image(image):
    basepath = path.dirname(path.abspath(__file__))
    return path.join(basepath, 'images', image)


FAIRGUIDE_DATA = {

    'title': 'Kontakt.24',

    'copies': 800,  # "Auflage"

    # Images
    'title_image': _image('title.jpg'),
    'back_image':  _image('back.jpg'),
    'filler_base': _image('filler'),
    'filler_number': 5,

    # Images for missing logos/ads
    'logo_missing': _image('logo_missing.png'),
    'ad_missing': _image('ad_missing.png'),
    'ad_ad': _image('ad_ad.png'),

    # Notes at the end (an additional note page is inserted if the specified
    # number would result in and odd page number)
    'note_page_number': 2,

    # 'back_image'
    'tuesday_left': _image('tuesday_left.jpg'),
    'tuesday_right': _image('tuesday_right.jpg'),
    'wednesday_left': _image('wednesday_left.jpg'),
    'wednesday_right': _image('wednesday_right.jpg'),

    # Fair days (solve this with system locales maybe)
    # TODOOOO
    'first_day': '8',
    'second_day': '9',
    'first_weekday_de': 'Dienstag',
    'first_weekday_en': 'Tuesday',
    'second_weekday_de': 'Mittwoch',
    'second_weekday_en': 'Wednesday',
    'month': '10',
    'year': '2024',
    'start': '11:00',
    'end': '17:00',

    'president': 'Elias Westrup, Kim Kleinlogel',
    'president_welcome_de': None,  # 2019: No intro in german
    'president_welcome_en': dedent("""
        Welcome to AMIV Kontakt.23!
        \medskip

        The AMIV Kontakt.23 organization committee and we welcome you to this year’s job fair. 
        The Kontakt is the annual job fair of the student associations OBIS and AMIV, organized for students by students for more than 20 years.
        \medskip

        There are more than 50 interesting companies from various industries presenting themselves this year. 
        We sincerely hope that you have enjoyable conversations with people from different working backgrounds and find the internship, job or thesis you have always been looking for. 
        If you don't yet know in what direction your career will take off in the future, take this opportunity to get an insight into the working world. 

        In preparation for your future applications, don’t forget to get some professional advice on your application documents at our free CV check, 
        as well as portraits at the photo booth.

        We would like to thank the entire AMIV Kontakt.23 organizational committee as well as all other volunteers that lent us a helping hand. 
        Thanks to their hard work throughout the past year and the fair itself we hope to give you the best possible experience for the Kontakt.23!
        """),
    'president_image': _image('pq_close.jpg'),

    # TODO
    'rector': 'Günther Dissertori',
    'rector_welcome_de': None,  # 2019: No intro in german
    'rector_welcome_en': dedent("""
        Dear students,
        \medbreak
        You are about to take your degrees and begin your professional lives. It is a wonderful opportunity to start to 
        make your mark on the world, with your newly minted qualification as an engineer from ETH! Your degree opens up 
        so many potential careers that it is sometimes a great challenge to be able to decide where to begin. There is 
        so much choice, so it is important to speak with potential employers, and their recent recruits, and find out 
        about their company.

        The AMIV Kontakt.23 event is not only an excellent opportunity to learn about a large number of well-known 
        employers, but it also provides many chances to hear about the experience of those who have already made the 
        transition from university to career.

        Visit AMIV Kontakt.23 to discover what fields the companies operate in, what you might expect in your future 
        working environment, and what you should pay attention to when you apply. Ask your conversation partners about 
        their entry into professional life, and the experiences that have remained with them. I am certain that you will
         enjoy many interesting encounters and discussions.

        I would like to show heartfelt appreciation to the Kontakt.23 team and the student associations, AMIV and OBIS, 
        for organising this event. Congratulations and thank you!

        I wish you all much success, joy and fulfilment in your future careers.
        """),
    'rector_image': _image('rector.jpg'),

    'booth_layout': _image('booth_layout.pdf'),

    'support_program': [
        ('Talk with Engineers Without Borders Switzerland (IngOG+),',
         '27.09.2021', '18:15', 'HG E 1.2', dedent(r"""
            \medbreak
            Meet the people behind Engineers Without Borders Switzerland (IngOG+),
            an association consisting of students and young professionals who
            voluntarily work on engineering projects around the world: from projects
            for social housing for women, drinking water for rural communities and
            schools to collaborations with foreign universities. Find out who we are,
            how we work and how to get involved!
            """)),
        ('Get ready for a job fair',
         '4.10.2021', '18:15', 'ML D 28', dedent(r"""
            \medbreak
            Are you ready to apply for the right job which fits your interests
            or are you overwhelmed by the endless application sites and don’t
            know how to get started? Maybe you are unsure if you are taking the
            right path? The Career Center will explain about job search
            strategies and decision making so you will know that you are making
            the right choices.
            """)),
        ('Lohnverhandlungen und Saläre',
         '7.10.2021', '18:15', 'HG E 1.1', dedent(r"""
            \medbreak
            \emph{Diese Veranstaltung findet in deutscher Sprache statt.}
            \medbreak
            Das Gehalt ist ein nicht zu unterschätzender Bestandteil des
            Jobangebots. Doch wie hoch ist ein klassisches Ingenieursgehalt
            wirklich? Swiss Engineering präsentiert Fakten zum Thema
            Lohnverhandlungen und Saläre und zu den damit verbundenen Aspekten.
            Damit beim nächsten Bewerbungsgespräch die Vorstellungen über ein
            angemessenen Lohn stimmen.
            """)),

    ],

    'cv_check_room': 'LEE E 308',
    'cv_check_intro_de': dedent(r"""
        Während der Messe sind mehrere Consultants
        der Consult \& Pepper AG anwesend
        und helfen, deinen CV auf Hochglanz
        zu polieren. Bring dazu einfach deinen
        ausgedruckten Lebenslauf mit.
        """),
    'cv_check_description_de': dedent("""
        Bei jeder Bewerbung ist der CV deine Visitenkarte.
        Er sollte auf alle Fragen zu deinen Qualifikationen, Erfahrungen
        und Kenntnissen beantworten und trotzdem kurz und prägnant sein.
        Häufig entscheidet eine erste Durchsicht der Bewerbungsdokumente
        durch den Arbeitgeber, ob die Kandidatin oder der Kandidat auf die
        zu besetzende Stelle passt oder nicht.
        Umso wichtiger ist es, dass dein CV ein professionelles
        Erscheinungsbild hat.

        Dabei helfen dir die Berater unseres Partners und unterstützen
        dich mit ihrer langjährigen Erfahrung.
        """),
    'cv_foto_de': dedent(r"""
        Zu jedem CV gehört auch ein professionelles
        Bewerbungsfoto. Mit deinem Profilbild
        hinterlässt du den ersten Eindruck. Deswegen ist es wichtig
        ein ansprechendes Foto zu haben, welches den Betrachter
        überzeugt. Deshalb bieten wir in Zusammenarbeit
        mit der Consult \& Pepper AG ein kostenloses Fotoshooting an.
        Eine Berufsfotografin rückt dich ins richtige
        Licht und wird dich von deiner besten
        Seite präsentieren.
        """),
    'cv_check_intro_en': dedent(r"""
        During the job fair, several consultants of our partner,
        the Consult \& Pepper AG, are available at your convenience
        and help to brush up your CV. Just bring a copy of it.
        """),
    'cv_check_description_en': dedent("""
        Your CV is the first impression an employer gets of you.
        It should show your qualifications, experiences and knowledge
        and still be comprehensive and concise.
        Oftentimes, the decision, if a candidate is invited for the
        interview, is made after an initial review of all the received
        CVs. It's all the more important that your CV has a professional
        appearance.

        The consultants of our partner support you with their experience
        of many years to stand out.
        """),
    'cv_foto_en': dedent("""
        Every CV requires a professional picture. With your profile,
        you leave the first impression before your CV or personal statement
        is read. This makes it all the more important to have an
        appealing picture to capture the viewer.

        Therefore, we offer a free fotoshooting with our partner, the
        Consult \& Pepper AG. A professional photographer will
        place you in the right light.
        """),

    # Pages for cv check company and career center
    'cv_check_company_id': 77,
    'career_center_id': 461,


    # Team
    'team_image': _image('team.jpg'),
    'kontakt_team': [
        ('Elias Westrup',
         _image('elias.jpg'),
         r'Co-Präsident\minilangsep Co-President'),
        ('Lars Meyer',
         _image('lars.jpg'),
         r'Co-Präsident\minilangsep Co-President'),
     	('Jeremias Baur',
         _image('jere.jpg'),
         r'IT'),
        ('George Motschan-Armen',
         _image('george.jpg'),
         r'Infrastruktur\minilangsep\\Infrastructure'),
        ('Leander Hoffmann',
         _image('leander.jpg'),
        r'Infrastruktur\minilangsep\\Infrastructure'),
        ('Martin Wonka',
         _image('der_andere_martin.jpg'),
         r'Catering'),
        ('Henriette Stadler',
         _image('hette.jpg'),
          r'Catering'),
        ('Florian Roth',
         _image('flo.jpg'),
          r'PR'),
        ('Mia Ritter (OBIS)',
         _image('mia.jpg'),
         r'PR')
    ],

    'er_team': [
        'Andreas Hirsch',
        'Jasmina Rui',
        'Daniel Gisler',
        'Till Häussner',
        'Robin Dörge',
        'Cecily Merkle',
        r'\vspace{5cm}',  # Hack to make columns look nice
    ],

    # Additional Data for info letter only
    'supervisor': 'Elias Westrup, Kim Kleinlogel',
    'supervisor_phone': '+49 000 00 00',

    'coffee_room': 'CLA D 19',
    'wifi_name': None,  # 'public',
    'wifi_user': None,  # 'kontakt.18',
    'wifi_password': None,  # 'AmivJobFair18',

    'map': _image('map.png'),

    'setup_start': '8:30',
    'setup_end': '11:00',
    'teardown_start': '17:00',
    'teardown_end': '18:00',
    'apero_start': '17:15',
    'apero_end': '19:30',

    # Data for CV Check Contract
    'cv_check_price': 1000,

}
