# -*- coding: utf-8 -*-

"""App settings."""

from os import path
from datetime import datetime as dt

# URL for amivapi and new kontakt website
AMIVAPI_URL = 'https://api-dev.amiv.ethz.ch/'
KONTAKTAPI_URL = 'https://api.kontakt.amiv.ethz.ch/contractor/'
KONTAKTAPI_KEY = 'supersecret'

# Development OAuth Id and redirect to default port of flask dev server
OAUTH_ID = 'Local Tool'
OAUTH_URI = 'http://localhost:5000'


# Whether compilation is cached; `False` is useful for working on templates
COMPILE_CACHE = False


# Yearly fair settings, move to CRM as soon as possible
YEARLY_SETTINGS = {
    'fairtitle': "AMIV Kontakt.24",
    # president of kontakt team
    'president': 'Elias Westrup, Kim Kleinlogel',
    # Sender of Contracts, usually treasurer of kontakt team
    'sender': 'Elias Westrup & Kim Kleinlogel \nKontakt Co-Presidents',

    # Fair days,
    'days': {
        'first': dt(2024, 10, 8),
        'second': dt(2024, 10, 9),
    },

    # Prices, all in francs
    'prices': {
        # Small
        'sA1': '1150',
        'sA2': '2500',
        'sB1': '850',
        'sB2': '1900',
        'sC1': '550',
        'sC2': '1300',
        # Big
        'bA1': '2400',
        'bA2': '5000',
        'bB1': '1800',
        'bB2': '3800',
        # Startups C und B
        'suC1': '250',
        'suC2': '1000',
        'susB1': '550',
        'susB2': '1600',
        # Pakets
        'media': '850',
        'business': '1500',
        'premium': '2500',
        'mediaPlus': '1500',
    },
}

# Download URLs for logos
LOGO_URL = 'http://www.kontakt.amiv.ethz.ch/data/company-logos/2016'
AD_URL = 'http://www.kontakt.amiv.ethz.ch/data/company-inserat'

# Paths to placeholder logo/ad
BASEPATH = path.join(path.dirname(path.abspath(__file__)), 'tex_templates')
LOGO_MISSING = path.join(BASEPATH, 'logo_missing.png')
AD_MISSING = path.join(BASEPATH, 'ad_missing.png')
AD_AD = path.join(BASEPATH, 'ad_ad.png')

# Maximum Dimensions for Logo
MAX_LOGO_DIMENSION = 2048

# Command to compress pdf (here with ghostscripts printer preset)
# We explicitly use bicubic downsampling to reduce artefacts in jpgs.
# "printed=false" preserves hyperlinks in the output.
COMPRESS_CMD = ("gs -sDEVICE=pdfwrite -dPDFSETTINGS=/ebook -dNOPAUSE -dBATCH "
                "-dColorImageDownsampleType=/Bicubic -dPrinted=false "
                "-sOutputFile={output} {input}")

# Command to convert svg to pdf (the svg is rasterized at 300 dpi)
# CONVERT_CMD = (
#     "inkscape --without-gui --export-dpi 300 "
#     "--file {input} --export-pdf {output}")
CONVERT_CMD = (
    "inkscape --export-dpi 300 --export-type pdf "
    "--export-filename {output} {input}")
