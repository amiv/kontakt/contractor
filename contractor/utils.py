# Utility functions

import hashlib
import json


def hash_data(data):
    """Reproducibly hash a dict/list/etc. and return hash string.

    First use json to dump to a string, while sorting, then hash with sha256.
    (As a result, datastructure needs to be json-compatible).
    """
    return hashlib.sha256(
        json.dumps(data, sort_keys=True).encode('utf-8')).hexdigest()
