# -*- coding: utf-8 -*-

"""The app."""
import json
import subprocess
from datetime import datetime as dt
from functools import wraps
from io import BytesIO
from locale import LC_TIME, setlocale
from os import getcwd, getenv, path, listdir, remove
from tempfile import NamedTemporaryFile
from babel.dates import format_datetime
from flask import (Flask, abort, g, make_response, render_template, request,
                   send_file, url_for, redirect)
from flask_cors import CORS
from jinja2 import PackageLoader, StrictUndefined
from raven.contrib.flask import Sentry
from jinjatex import Jinjatex
from werkzeug.exceptions import NotFound
from schema import SchemaError

from contractor.api_auth import logout as api_logout
from contractor.api_auth import protected
from contractor.fairguide_data import FAIRGUIDE_DATA  # TODO
from contractor.kontakt_api import get_companies, get_company_list
from contractor.utils import hash_data

app = Flask('contractor')
app.config.from_pyfile('settings.py')

# Try to load config specified by envvar, default to config.py in work dir
default_config = path.join(getcwd(), 'config.py')
config_file = getenv("CONTRACTOR_CONFIG", default_config)
print("Attempting to load configuration:", config_file, flush=True)

app.config.from_pyfile(config_file)

# If directories have not been defined, use '.cache' in current working dir
app.config.setdefault('STORAGE_DIR', path.abspath('./.cache'))

# Set locale to ensure correct weekday format
app.config.setdefault('LOCALE', 'de_CH.utf-8')
setlocale(LC_TIME, app.config['LOCALE'])

try:
    sentry = Sentry(app, dsn=app.config['SENTRY_DSN'])
except KeyError:
    print("Sentry inactive: 'SENTRY_DSN' not found in config.", flush=True)

# add CORS handling
CORS(app)

TEX = Jinjatex(tex_engine='xelatex',
               loader=PackageLoader('contractor', 'tex_templates'),
               undefined=StrictUndefined,
               trim_blocks=True)


def fairday(companies, day):
    """Filter based on participation day."""
    def _filter(company):
        if 'participation' in company and 'booths' in company['participation']:
            for booth in company['participation']['booths']:
                days = booth['day']
                if ((days in ['first', 'both']) if (day == 'first') else (days in ['second', 'both'])):
                    return True
        return False
    return [company for company in companies if _filter(company)]

def extract_booths(companies, day):
    """Extract booths and sort them according to the calculated booth index.
    Returns location and company_name only!"""
    data = []

    for company in companies:
        if 'participation' in company and 'booths' in company['participation']:
            for booth in company['participation']['booths']:
                if ((booth['day'] in ['first', 'both']) if (day == 'first') else (booth['day'] in ['second', 'both'])):
                    data.append({
                        'booth_index': booth.get('booth_index'),
                        'location': booth.get('location'),
                        'company_name': company.get('name')
                    })
    return data

def extract_booth_location(company, day):
    """Extract booth location(s) for the given day."""
    locations = []
    if 'participation' in company and 'booths' in company['participation']:
        for booth in company['participation']['booths']:
            days = booth['day']
            is_on_selected_day = (days in ['first', 'both']) if (day == 'first') else (days in ['second', 'both'])
            if is_on_selected_day and booth['location'] is not 'None':
                locations.append(booth['location'])

    if len(locations) > 0:
        return '/'.join(locations)
    return None


TEX.env.filters.update({
    # Filters to parse date, including short one to list dates nicely
    # Format: Dienstag, 18.10.2016
    'fulldate': lambda date: dt.strftime(date, "%A, %d.%m.%Y"),
    # Format: Dienstag, 18.10.2016
    'fulldate_de': lambda date: format_datetime(date, 'EEEE, d.MM.yyyy', locale='de'),
    # Format: Tuesday, 2016-10-18
    'fulldate_en': lambda date: format_datetime(date, 'EEEE, yyyy-MM-d', locale='en'),
    # Format: Dienstag, 18.
    'shortdate': lambda date: dt.strftime(date, "%A, %d."),
    # Format: Dienstag, 18.
    'shortdate_de': lambda date: format_datetime(date, 'EEEE, d.', locale='de'),
    # Format: Tuesday, 18.
    'shortdate_en': lambda date: format_datetime(date, 'EEEE, d.', locale='en'),
    # Check whether a company is present on a day: day("first") or day("second")
    'day': fairday,
    # Extracts booth-company name data pair from the company data
    'extract_booths': extract_booths,
    # Extracts all booth locations for a company on the given day
    'extract_booth_location': extract_booth_location,
})


def _file_response(data, filename, mimetype):
    """Make file response with appropriate headers to disable caching."""
    response = make_response(send_file(BytesIO(data),
                                       mimetype=mimetype,
                                       download_name=filename,
                                       as_attachment=True))
    response.headers['Content-Length'] = len(data)
    return response


def _get_meta(output_format, basename=None):
    """Return filename and mimetype based on requested format."""
    if output_format != 'tex':
        filename = '%s.pdf' % g.get('company', basename or 'document')
        return (filename, 'application/pdf')
    else:
        filename = '%s.tex' % g.get('company', basename or 'source')
        return (filename, 'text/plain')


def _get_data(output_format, template, *args, **kwargs):
    """Prepare data, compress pdfs if needed."""
    if output_format != 'tex':
        data = TEX.compile_template(template, *args, **kwargs)

        # Compress the pdf if needed
        if output_format == 'compressed':
            with NamedTemporaryFile() as uncompressed:
                with NamedTemporaryFile() as compressed:
                    uncompressed.write(data)

                    arg_string = app.config['COMPRESS_CMD'].format(
                        input=uncompressed.name,
                        output=compressed.name)

                    subprocess.run(arg_string.split(), check=True)
                    data = compressed.read()

        return data
    else:
        data = TEX.render_template(template, *args, **kwargs)
        return data.encode()


# Routes

@app.route('/', methods=['GET', 'POST'])
@protected
def main():
    """Main view.

    Includes output format and yearly settings.
    """
    companies = get_company_list()

    return render_template('main.html',
                           user=g.get('username', ''),
                           yearly=app.config['YEARLY_SETTINGS'],
                           companies=companies)


@app.route('/clear', methods=['GET', 'POST'])
@protected
def clear_cache():
    """Clear cache, and redirect back to main."""
    # Remove dir and all contents, don't complain if directory is missing.
    try:
        files = listdir(app.config['STORAGE_DIR'])
    except FileNotFoundError:
        pass
    else:
        for file in files:
            remove(path.join(app.config['STORAGE_DIR'], file))

    return redirect(url_for('main'))


@app.route('/logout')
def logout():
    """Log out."""
    return api_logout('You have been logged out. Goodbye!')


@app.route('/custom/', methods=['GET', 'POST'])
@protected
def custom():
    """View to show and create customizable letter."""
    fields = ['destination_address', 'subject', 'opening', 'body', 'closing',
              'signature', 'attachments']
    options = {field: request.form.get(field, '') for field in fields}

    # Fields must not be empty
    empty_ok = ['attachments']
    errors = {field: ((request.method == 'POST') and
                      (field not in empty_ok) and (not value))
              for field, value in options.items()}

    if request.method == 'POST' and not any(errors.values()):
        return _get_data('.pdf', 'custom_letter.tex', **options)

    return render_template('custom.html',
                           user=g.username,
                           values=options,
                           errors=errors)


def download_endpoint(endpoint_name, protect=True, individual=True):
    """Wrap the function as a flask endpoint.

    Accepts:
    <endpoint_name>/output_format/
    <endpoint_name>/output_format/company_id  (if individual=True)

    Companies are retrieved (without company id, all companies are fetched)
    and the function is called with:

    function(output_format, list_of_companies)
    """

    def _wrapper(func):
        @wraps(func)
        def _temp(output_format, company_id=None, **kwargs):
            try:
                companies = get_companies(company_id)
            except NotFound:
                abort(404, "The Kontakt API returned no data!")
            except SchemaError as error:
                abort(500, "Parsing the Kontakt API data failed: '%s'" % error)

            if not companies:
                abort(404)

            if app.config['COMPILE_CACHE']:
                # If possible, get data from cache
                cache_path = path.join(
                    app.config['STORAGE_DIR'],
                    '%s.%s' % (hash_data(companies), output_format))
                try:
                    with open(cache_path, 'rb') as file:
                        data = file.read()
                except FileNotFoundError:
                    # Execute function and cache for next time
                    data = func(output_format, companies, **kwargs)
                    with open(cache_path, 'wb') as file:
                        file.write(data)
            else:
                data = func(output_format, companies, **kwargs)

            # Send data
            name, mimetype = _get_meta(output_format)
            return _file_response(data, name, mimetype)

        # Add url rules for the prepared function
        _final = protected(_temp) if protect else _temp
        app.add_url_rule('/%s/<output_format>' % endpoint_name,
                         view_func=_final)
        if individual:
            app.add_url_rule(
                '/%s/<output_format>/<company_id>' % endpoint_name,
                view_func=_final)

        return _final
    return _wrapper


@download_endpoint('confirmations')
def confirmations(output_format, companies):
    """Confirmation creation."""
    # Get yearly settings
    yearly = app.config['YEARLY_SETTINGS']
    return _get_data(output_format, 'confirmation.tex',
                     companies=companies, **yearly)


@download_endpoint('fairguide', individual=False)
def fairguide(output_format, companies):
    """Create the fairguide."""
    cv_company = get_companies(FAIRGUIDE_DATA['cv_check_company_id'],
                               ignore_participation=True)[0]
    career_center = get_companies(FAIRGUIDE_DATA['career_center_id'],
                                  ignore_participation=True)[0]

    return _get_data(output_format,
                     'fairguide.tex',
                     print=(output_format == 'print'),
                     companies=companies,
                     cv_check_company=cv_company,
                     career_center=career_center,
                     **FAIRGUIDE_DATA)


# Unprotected, companies need to access it!
@download_endpoint('fairguidepage', protect=False)
def fairguidepage(output_format, companies):
    """Return the rendered page for a single company.

    The page is created based on company data proviced by the kontakt api
    and is cached.
    """
    return _get_data(output_format, 'company_page.tex',
                     companies=companies,
                     **FAIRGUIDE_DATA)


@download_endpoint('infoletter')
def infoletter(output_format, companies):
    """Create the information letter for the fair days."""
    return _get_data(output_format,
                'infoletter.tex',
                companies=companies,
                     fair_companies=get_companies(),
                **FAIRGUIDE_DATA)


@app.route('/boothplan/<output_format>')
@protected
def booth_plan(output_format):
    """Create the information letter for the fair days."""
    data = _get_data(output_format,
                'boothplan.tex',
                     fair_companies=get_companies(),
                **FAIRGUIDE_DATA)

    name, mimetype = _get_meta(output_format, basename='boothplan')
    return _file_response(data, name, mimetype)


@protected
@app.route('/cvcontract/<output_format>')
def cv_contract(output_format):
    """Create the contract for the cv check company."""
    cv_company = get_companies(FAIRGUIDE_DATA['cv_check_company_id'],
                               ignore_participation=True)[0]

    data = _get_data(output_format,
                     'cvcontract.tex',
                     company=cv_company,
                     **FAIRGUIDE_DATA)

    name, mimetype = _get_meta(output_format, basename='cvcontract')
    return _file_response(data, name, mimetype)


@download_endpoint('feedback')
def feedback(output_format, companies):
    """Create the information letter for the fair days."""
    return _get_data(output_format,
                'feedback.tex',
                companies=companies,
                title=FAIRGUIDE_DATA['title'])
