FROM amiveth/amivtex

# Ensure the de_CH.utf-8 locale exists, needed for weekday mapping
RUN apt-get update && apt-get install -y locales && \
    echo "de_CH.UTF-8 UTF-8" >> /etc/locale.gen && locale-gen && \
    rm -rf /var/lib/apt/lists/*

# Webserver (bjoern) and dependencies
RUN apt-get update && apt-get install -y \
    musl-dev python-dev gcc libev-dev && \
    pip install bjoern

# PDF compression
RUN apt-get update && apt-get install -y ghostscript

# SVG to PDF Conversion
RUN apt-get update && apt-get install -y inkscape

# Create user with home directory and no password and change workdir
RUN useradd -md /contractor contractor
WORKDIR /contractor
# Run on port 8080 (does not require priviledge to bind)
EXPOSE 8080
# Environment variable for config
ENV CONTRACTOR_CONFIG=/contractor/config.py


# Install requirements
# libpng and libjpg are needed for Pillow only
RUN apt-get update && apt-get install -y libpng-dev libjpeg62-turbo-dev && \
    tlmgr update --self && \
    tlmgr install crop
COPY ./requirements.txt /contractor/requirements.txt
RUN pip install -r /contractor/requirements.txt

# Copy other files
COPY ./ /contractor

# Switch user
USER contractor

# Modified entrypoint to load font url from app config
ENTRYPOINT ["/contractor/contractor_entrypoint.sh"]


# Start bjoern
CMD ["python3", "server.py"]
